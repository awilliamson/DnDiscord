import yaml
from typing import Dict, Any, Optional


class Config:
    def __init__(self, path: str = "config.yaml") -> None:
        self.path: str = path
        self.cfg: Optional[Dict[str, Any]] = None

    def get_config(self) -> Optional[Dict[str, Any]]:
        if self.cfg is None:
            with open(self.path, "r") as cfg:
                self.cfg = yaml.load(cfg)

        return self.cfg
