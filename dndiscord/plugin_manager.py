import datetime
from importlib.util import module_from_spec, spec_from_file_location
from pathlib import Path
from typing import (Any, Awaitable, Callable, Dict, List, NamedTuple, Optional,
                    Union)

import discord

CommandHandlerFunc = Callable[[discord.Message], Awaitable[None]]


class CommandHandler(NamedTuple):
    command: str
    handler: CommandHandlerFunc
    delete_command: bool


class Plugin(NamedTuple):
    identifier: str
    command_handlers: List[str]
    module: Any


class PluginManager(object):
    plugins: Dict[str, Plugin] = {}
    command_handlers: Dict[str, CommandHandler] = {}
    client: Optional[discord.Client] = None
    _plugin_being_loaded: Optional[str] = None

    def __init__(self) -> None:
        raise RuntimeError()

    @staticmethod
    def read_plugins(path: Path) -> None:
        for fil in path.iterdir():  # type: Path
            if not fil.is_file():
                continue
            if not fil.name.endswith(".py"):
                continue
            plugin_id = fil.name[:-3]
            spec = spec_from_file_location(plugin_id, str(fil.resolve()))
            module = module_from_spec(spec)
            PluginManager.plugins[plugin_id] = Plugin(plugin_id, [], module)

            PluginManager._plugin_being_loaded = plugin_id
            spec.loader.exec_module(module)
            PluginManager._plugin_being_loaded = None

    @staticmethod
    async def on_ready() -> None:
        pass

    @staticmethod
    async def on_resumed() -> None:
        pass

    @staticmethod
    async def on_error(event: str, *args, **kwargs) -> None:
        pass

    @staticmethod
    async def on_message(message: discord.Message) -> None:
        if message.content.startswith("!"):
            cmd = message.content[1:].split(" ")[0]
            if cmd in PluginManager.command_handlers:
                handler = PluginManager.command_handlers[cmd]
                await handler.handler(message)
                if handler.delete_command:
                    await message.delete()

    @staticmethod
    async def on_socket_raw_receive(message: Union[str, bytes]) -> None:
        pass

    @staticmethod
    async def on_socket_raw_send(payload: Union[str, bytes]) -> None:
        pass

    @staticmethod
    async def on_message_delete(message: discord.Message) -> None:
        pass

    @staticmethod
    async def on_message_edit(before: discord.Message, after: discord.Message) -> None:
        pass

    @staticmethod
    async def on_message_add(reaction: discord.Reaction, user: Union[discord.User, discord.Member]) -> None:
        pass

    @staticmethod
    async def on_reaction_remove(
        reaction: discord.Reaction, user: Union[discord.User, discord.Member]
    ) -> None:
        pass

    @staticmethod
    async def on_reaction_clear(message: discord.Message, reactions: List[discord.Reaction]) -> None:
        pass

    @staticmethod
    async def on_channel_delete(channel: Union[discord.TextChannel, discord.VoiceChannel]) -> None:
        pass

    @staticmethod
    async def on_channel_create(channel: Union[discord.TextChannel, discord.VoiceChannel]) -> None:
        pass

    @staticmethod
    async def on_channel_update(
        before: Union[discord.TextChannel, discord.VoiceChannel],
        after: Union[discord.TextChannel, discord.VoiceChannel],
    ) -> None:
        pass

    @staticmethod
    async def on_member_join(member: discord.Member) -> None:
        pass

    @staticmethod
    async def on_member_remove(member: discord.Member) -> None:
        pass

    @staticmethod
    async def on_member_update(before: discord.Member, after: discord.Member) -> None:
        pass

    @staticmethod
    async def on_server_join(server: discord.Guild) -> None:
        pass

    @staticmethod
    async def on_server_remove(server: discord.Guild) -> None:
        pass

    @staticmethod
    async def on_server_update(before: discord.Guild, after: discord.Guild) -> None:
        pass

    @staticmethod
    async def on_server_role_create(role: discord.Role) -> None:
        pass

    @staticmethod
    async def on_server_role_delete(role: discord.Role) -> None:
        pass

    @staticmethod
    async def on_server_role_update(before: discord.Role, after: discord.Role) -> None:
        pass

    @staticmethod
    async def on_server_emojis_update(before: List[discord.Emoji], after: List[discord.Emoji]) -> None:
        pass

    @staticmethod
    async def on_server_available(server: discord.Guild) -> None:
        pass

    @staticmethod
    async def on_server_unavailable(server: discord.Guild) -> None:
        pass

    @staticmethod
    async def on_voice_state_update(before: discord.Member, after: discord.Member) -> None:
        pass

    @staticmethod
    async def on_member_ban(member: discord.Member) -> None:
        pass

    @staticmethod
    async def on_member_unban(server: discord.Guild, user: discord.User) -> None:
        pass

    @staticmethod
    async def on_typing(
        channel: discord.TextChannel, user: Union[discord.User, discord.Member], when: datetime.datetime
    ) -> None:
        pass

    @staticmethod
    async def on_group_join(
        channel: Union[discord.TextChannel, discord.VoiceChannel], user: discord.User
    ) -> None:
        pass


def command(cmd: str, delete_message: bool = True):
    def decorator(func: CommandHandlerFunc) -> CommandHandlerFunc:
        def nop(*args, **kwargs):
            pass

        if cmd in PluginManager.command_handlers:
            # Hmm... what do?
            return nop

        handler = CommandHandler(cmd, func, delete_message)
        PluginManager.command_handlers[cmd] = handler
        PluginManager.plugins[PluginManager._plugin_being_loaded].command_handlers.append(cmd)
        return func

    return decorator
