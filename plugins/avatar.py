import asyncio

import discord
from dndiscord.plugin_manager import command

META = {
    "name": "Avatar",
    "version": "1.0",
    "author": "Ashley Williamson",
    "description": "Get the avatar and its URL from the specified user",
}


@command("avatar", delete_message=False)
async def avatar(message: discord.Message):

    if len(message.mentions) > 0:

        async def avatar_and_delete(user):
            msg = await message.channel.send(
                "Deleting in 5s\n<@{}>'s Avatar URL: {}".format(user.id, user.avatar_url)
            )
            await asyncio.sleep(5)
            await msg.delete()

        tasks = asyncio.gather(*[asyncio.ensure_future(avatar_and_delete(m)) for m in message.mentions])
        await message.delete()
        asyncio.wait_for(tasks, 15)
