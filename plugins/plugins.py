from typing import Awaitable, Tuple  # noqa

import discord
from dndiscord.plugin_manager import PluginManager, command

META = {
    "name": "Plugin manager",
    "version": "1.0",
    "author": "Xandaros",
    "description": "Used to load/unload/reload/suspend/unsuspend plugins (WIP)",
}


@command("plugins")
async def plugins(msg: discord.Message) -> None:
    response = discord.Embed(title="Plugins", type="rich")
    for plugin_id, plugin in PluginManager.plugins.items():
        meta = plugin.module.META
        response.add_field(
            name="{} ({})".format(meta["name"], plugin_id),
            value="{} (Author: {})".format(meta["description"], meta["author"]),
            inline=False,
        )
    await msg.channel.send(content=None, embed=response)
