import discord
from dndiscord.plugin_manager import command

META = {
    "name": "Test plugin",
    "version": "1.0",
    "author": "Xandaros",
    "description": "Plugin to test various aspects of the plugin system",
}


@command("plugintest")
async def plugintest(message: discord.Message):
    await message.channel.send("Boopity")
